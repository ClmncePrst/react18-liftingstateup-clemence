import "./Navigation.css";

function Navigation(props) {
  // TODO get user from props drilling en déstructurant la prop 'user' depuis l'objet 'props'
  const { user } = props;
  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/* TODO check if user is connected: utilisation d'un opérateur ternaire pour afficher le nom
      de l'utilisateur si celui-ci est renseigné ou une demande de connexion sinon  */}
      <li> {user ? `Username: ${user}` : 'Please log in'}</li>
    </ul>
  );
}

export default Navigation;
