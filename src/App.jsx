import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  const [user, setUser] = useState(null);

  return (
    <div className="App">
      {/* Composant Navigation avec utilisation de la prop 'user' */}
      <Navigation user={user} />
      {/* Composant Profile avec utilisation des prop 'user' et 'setUser' */}
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

export default App;
